FROM golang

ADD main.go /home/

RUN go build /home/main.go

ENTRYPOINT /go/main

EXPOSE 8080